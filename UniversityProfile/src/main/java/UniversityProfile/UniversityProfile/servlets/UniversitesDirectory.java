package UniversityProfile.UniversityProfile.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import UniversityProfile.UniversityProfile.utils.PostgresConn;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class UniversitesDirectory
 */
@WebServlet("/UniversitesDirectory")
public class UniversitesDirectory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UniversitesDirectory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			ArrayList<String> objJsonArray=UniversitesDirectory.gettingDirectoris("A");
			response.setContentType("application/text");
			PrintWriter out = response.getWriter();
			out.print(objJsonArray);
			out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	public static ArrayList<String> gettingDirectoris(String startswith) throws SQLException {
		ArrayList objJsonArrayObject = new ArrayList();
		if (startswith == null || startswith.isEmpty())
			startswith = "A";

		String sql = "SELECT uni_name, linkedin_link, icu_link FROM university where uni_name like '"
				+ startswith + "%'; ";
		Connection conn = PostgresConn.getConnection("University");
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			System.out.println("flksdjflksjldfjl");
			objJsonArrayObject.add(rs.getString(1));
		}
		return objJsonArrayObject;
	}
}
