package UniversityProfile.UniversityProfile.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import UniversityProfile.UniversityProfile.model.UniversityDetailsTo;

/**
 * Servlet implementation class UniversityServlet
 */
@WebServlet("/UniversityServlet")
public class UniversityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UniversityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String collegeName = request.getParameter("param");
		System.out.println("RRRRRRRRRRRRRRRRRRRRRRRR"+collegeName);
		UniversityDetailsTo objUniversityDetailsTo=new UniversityDetailsTo();
		objUniversityDetailsTo.setUniversityName(collegeName);
		objUniversityDetailsTo.setAbout("Founded in 1892, A.T. Still University is a non-profit private higher education institution located in the the rural setting of the medium-sized town of Kirksville (population range of 10,000-49,999 inhabitants), Missouri. This institution has also branch campuses in the following location(s): Mesa. Officially accredited/recognized by the The Higher Learning Commission of the North Central Association of Colleges and Schools, A.T. Still University (ATSU) is a small (enrollment range: 4,000-4,999 students) coeducational higher education institution. A.T. Still University (ATSU) offers courses and programs leading to officialy recognized higher education degrees such as master degrees, doctorate degrees in several areas of study. This 124 years old HE institution has a selective admission policy based on entrance examinations and students' past academic record and grades. International students are welcome to apply for enrollment. ATSU also provides several academic and non-academic facilities and services to students including a library, housing, sport facilities and/or activities, financial aids and/or scholarships, online courses and distance learning opportunities, as well as administrative services.");
		objUniversityDetailsTo.setFoundedYear("1892");
		objUniversityDetailsTo.setAddress("800 W Jefferson St.Kirksville 63501-1497 Missouri United States");
		objUniversityDetailsTo.setUniversityLink("http://www.devkalgudi.vasudhaika.net");
		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    out.println("<html>");
	    out.println("<head>");
	    out.println("<title>UniversityDetails</title>");
	    out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/Test.css\">");
	    out.println("</head>");
	    out.println("<body>");
	    out.println("<header><div><h2 align=\"center\"><a href="+objUniversityDetailsTo.getUniversityLink()+">"+objUniversityDetailsTo.getUniversityName()+"</a></h2></div></header>");
	    if(objUniversityDetailsTo!=null){
	    if(objUniversityDetailsTo.getAbout()!=null&&!objUniversityDetailsTo.getAbout().isEmpty()){
	    	out.println("<div>");
	    	out.println("<h3>About</h3>");
	    	out.println("<p>"+objUniversityDetailsTo.getAbout()+"</p>");
	    	out.println("</div>");
	    }
	    if(objUniversityDetailsTo.getFoundedYear()!=null&&!objUniversityDetailsTo.getFoundedYear().isEmpty()){
	    	out.println("<div>");
	    	out.println("<h3> Founded Year :</h3>");
	    	out.println("<p>"+objUniversityDetailsTo.getFoundedYear()+"</p>");
	    	out.println("</div>");
	    }
	    if(objUniversityDetailsTo.getAddress()!=null&&!objUniversityDetailsTo.getAddress().isEmpty()){
	    	out.println("<div>");
	    	out.println("<h3> Address :</h3>");
	    	out.println("<p>"+objUniversityDetailsTo.getAddress()+"</p>");
	    	out.println("</div>");
	    }
	    }
	    else{
	    	 out.println("<h3>sorry no data found</h3>");
	    }
	    out.println("</body>");
	    out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
