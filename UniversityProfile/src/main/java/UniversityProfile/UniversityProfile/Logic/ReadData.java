package UniversityProfile.UniversityProfile.Logic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import UniversityProfile.UniversityProfile.utils.PostgresConn;

public class ReadData {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReadData.class);
	public void getData(){
		Connection conn = PostgresConn.getConnection("University");
		try {
			Statement stmt = conn.createStatement();
			ResultSet objRs = stmt.executeQuery("select * from university");
			if(objRs.next()){
				LOGGER.error(""+objRs.getString("universityName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void main(String... args){
		new ReadData().getData();
	}
}
