package UniversityProfile.UniversityProfile.model;

public class UniversityDetailsTo {
	private String universityName;
	private String foundedYear;
	private String about;
	private String address;
	private String universityLink;

	public String getUniversityLink() {
		return universityLink;
	}

	public void setUniversityLink(String universityLink) {
		this.universityLink = universityLink;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public String getFoundedYear() {
		return foundedYear;
	}

	public void setFoundedYear(String foundedYear) {
		this.foundedYear = foundedYear;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
