package UniversityProfile.UniversityProfile.utils;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgresConn {
	//private static final Logger LOGGER = LoggerFactory.getLogger(PostgresConn.class);
	static String url = "jdbc:postgresql://192.168.1.87:5432/";
	public static Connection getConnection(String dbname) {
		Connection dbConnection = null;
		try {
			Class.forName("org.postgresql.Driver");
			dbConnection = DriverManager.getConnection(url + dbname, "openerp", "openerp");
		} catch (Exception e) {
			e.printStackTrace();
//			LOGGER.error("Exception occured while connecting to postgres");
		}
		return dbConnection;
	}
}
